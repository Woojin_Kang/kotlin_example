package my.kotlin.mystudy

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import my.kotlin.mystudy.javainterlop.javaActivity
import my.kotlin.mystudy.step1.*
import my.kotlin.mystudy.step2.*
import my.kotlin.mystudy.step3.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

                //예제들을 이곳에서 실행
//        doTest(FirstTest(::WriteLn))
//        doTest(NumberTest(::WriteLn))
//        doTest(StringTest(::WriteLn))
//        doTest(AnyTest(::WriteLn))
//        doTest(ConditionTest(::WriteLn))
//        doTest(LabelTest(::WriteLn))
//        doTest(CollectionsTest(::WriteLn))
//        doTest(ExceptionTest(::WriteLn))
//        doTest(ClassTest(::WriteLn))
//        doTest(PolyTest(::WriteLn))
//        doTest(InterAbstractTest(::WriteLn))
//        doTest(DataClassTest(::WriteLn))
//        doTest(ObjectTest(::WriteLn))
//        doTest(LambdasTest(::WriteLn))
//        doTest(ExtFuncTest(::WriteLn))
//        doTest(ExtFunc2Test(::WriteLn))
//        doTest(DSLTest(::WriteLn))
//        doTest(CurryngTest(::WriteLn))
//        doTest(InfixTest(::WriteLn))
//        doTest(MultiReturnTest(::WriteLn))
//        doTest(InitTest(::WriteLn))
        doTest(PropertiesTest(::WriteLn))

        btnTest.setOnClickListener {
            var I = Intent(this, javaActivity::class.java)
            startActivity(I)
        }
    }

    private fun doTest(o: TestClass) {
        o.doTest()
    }

    fun WriteLn(any: Any) {
        txtMessage.text = "${txtMessage.text}${any.toString()}\n"
    }
}
